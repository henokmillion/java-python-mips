# states

states = ["INSLCOMMENT", "INMLCOMMENT", "NORMAL", "CLASSLEVEL",
          "INMETHOD", "INSTRING", "INIF", "INMETHODARG"]
classStates = dict(zip(["file", "class", "method", "if", "else", "for", "while", "switch", "case", "comment", "braces","statment"], [
                   "IN_FILE", "IN_CLASS", "IN_METHOD", "IN_IF", "IN_ELSE", "IN_FOR", "IN_WHILE", "IN_SWITCH", "IN_CASE", "IN_COMMENT", "IN_BRACES", "IN_STATMENT"]))
