import re as regEx
from StatmentTypes import StatmentTypes

class HandleStatments:
    global newLine, data, instruction, addInstr, regUsed, paramReg, regFree, regAssignedVar, varInfo, tempoffset, tab

    def __init__(self):
        self.data = []
        self.instruction = []
        self.addInstr = []
        self.resetReg()
        self.newLine = "\n"
        self.tab = "\t"


    def interprateStatment(self, statment):
        self.data = []
        self.instruction = []
        type = self.classifyTypeOfStatment(statment)
        self.buildData(statment, type)
        self.buildInstruction(statment, type)
        return [self.data, self.instruction]


    def buildData(self, statment, type):
        if (type == StatmentTypes["print"]):
            identifier = regEx.search("\"(.*?)\"", statment, 0).group(1).split(" ")
            string = regEx.search("\"(.*?)\"", statment, 0).group(1)
            dataInst = self.tab + identifier[0] + ": .ascizz \"" + string + "\""
            self.data.append(dataInst)
            self.data.append(identifier[0])
            print self.data

        elif (type == StatmentTypes["closingBraceses"]):
            return 

        elif (type == StatmentTypes["comment"]):
            return
        
        elif (type == StatmentTypes["assignment"]):
            identifier = regEx.search("(.*)\s=", statment,0).group(1)
            data = regEx.search("=\s(.*);", statment,0).group(1)
            string = identifier + ": .asciiz \"" + data + "\""
            self.data.append(string)

        elif (type == StatmentTypes["arthimetic"]):
            print statment

    def buildInstruction(self, statment, type):
        instruction = ""
        if (type == StatmentTypes["print"]):
            instruction += self.tab + "li, " + "$vo, " + "4"
            instruction += self.newLine
            instruction += self.tab + "la, " + "$a0, " + self.data[1]
            instruction += self.newLine
            instruction += self.tab + "syscall"
            instruction += self.newLine
            self.instruction.append(instruction)
            self.data.pop()

        elif (type == StatmentTypes["closingBraceses"]):
            return

        elif (type == StatmentTypes["comment"]):
              comment = self.tab + "# " + regEx.search("(\/\/)(.*)", statment).group(2)
              comment += self.newLine
              self.instruction.append(comment)

        elif (type == StatmentTypes["arthimetic"]):
            print statment
        
        elif (type == StatmentTypes["assignment"]):
            return        


    def classifyTypeOfStatment(self, statment):
        if (regEx.search("System.out.print", statment, 0)):
            return StatmentTypes["print"]

        elif(regEx.search("}", statment, 0)):
            return StatmentTypes["closingBraceses"]

        elif (regEx.search("//", statment, 0)):
            return StatmentTypes["comment"]

        elif (regEx.search("[\+\-\/\*]",statment, 0)):
            return StatmentTypes["arthimetic"]

        elif (regEx.search("=", statment, 0)):
            return StatmentTypes["assignment"]
        
    # def addToString(self,var,stringToStore):
	# 	# print stringToStore
	# 	self.addInstr(['.data','','',''])
	# 	self.addInstr([var +':','.asciiz',stringToStore,''])
	# 	self.addInstr(['.text','','',''])
    
    def resetReg(self):
		self.registors = {
		'$0' : 0,				#$0		# always contains zero
		'$at': None , 			#$1		# assembler temporary
		'$v0': None , 			#$2		# Values from expression evaluation and function results
		'$v1': None ,			#$3		# Same as above
		'$a0': None ,			#$4		# First four parameters for function call 
		'$a1': None ,			#$5		# Not preserved across function calls
		'$a2': None , 			#$6		#
		'$a3': None , 			#$7		#
		'$t0': None ,			#$8		# (temporaries) Caller saved if needed.
		'$t1': None ,			#$9		# Subroutines can use w/out saving.
		'$t2': None ,			#$10	# Not preserved across procedure calls
		'$t3': None ,			#$11	#
		'$t4': None ,			#$12	#
		'$t5': None ,			#$13	#
		'$t6': None ,			#$14	#
		'$t7': None ,			#$15	#
		'$s0': None , 			#$16	# (saved values) - Callee saved. 
		'$s1': None , 			#$17	# A subroutine using one of these must save original 
		'$s2': None , 			#$18	# and restore it before exiting.
		'$s3': None , 			#$19	# Preserved across procedure calls.
		'$s4': None , 			#$20	# 
		'$s5': None , 			#$21	# 
		'$s6': None , 			#$22	# 
		'$s7': None , 			#$23	#
		'$t8': None ,			#$24	# (temporaries) Caller saved if needed. Subroutines can use w/out saving.
		'$t9': None ,			#$25	# Not preserved across procedure calls.
		'$k0': None , 			#$26	# reserved for use by the interrupt/trap handler
		'$k1': None , 			#$27	# 
		'$gp': None , 			#$28	# Global pointer. Points to the middle of the 64K block of memory in the static data segment.
		'$sp': None , 			#$29	# stack pointer 
		'$fp': None , 			#$30	# frame pointer, preserved across procedure calls
		'$ra': None ,			#s31	# return address
		}
		self.regUsed = []
		self.paramReg = ['$a0','$a1','$a2','$a3']
		self.regFree = ['$t0', '$t1','$t2','$t3','$t4','$t5','$t6','$t7','$s0', '$s1','$s2','$s3','$s4','$s5', '$s6']
		self.regAssignedVar = {}
		self.varInfo = {}
		self.tempoffset = 0