import re as regEx
from Stack import *
from HandleStatments import *
from states import classStates
from MIPSSuppotedDataTyes import supportedDataTypes
from StatmentTypes import StatmentTypes



class Interpretor:
    global filepath, braceStack, stateStack, stmHand, dataSection, textSection
    
    def __init__(self, filepath):
        self.filepath = filepath
        self.braceStack = Stack()
        self.stateStack = Stack()
        self.stmHand = HandleStatments()
        self.dataSection = []
        self.textSection = []

    def classifyLine(self, statement):

        # CLASS_STATE
        if (regEx.search("class", statement, 0)):
            return classStates['class']
        
        # METHOD_STATE
        for dataType in supportedDataTypes:
            if (regEx.search(" "+dataType, statement, 0) != None):
                if (regEx.search("\((?!r)", statement, 0) != None):
                    return classStates['method']

        # IF_STATE 
        if (regEx.search("if", statement, 0)):
            self.stateStack.push(classStates["if"])
            return classStates['if']
            

        # ELSE_STATE
        if (regEx.search("else", statement, 0)):
            self.stateStack.push(classStates["else"])
            return classStates['else']
        
        # FILE_STATE
        if (regEx.search("package", statement, 0)):
            return 
        
        # STATMENT
        return classStates['statment']
            

    def interprateLines(self, readLines):
        i = 0
        for line in readLines:
            readLines[i] = line.strip()
            print "Line Number: ------------------------------------------- [" + str(i+1) + "]"

            lineType = self.classifyLine(line)

            if (lineType == classStates["class"]):
                self.stateStack.push(classStates['class'])
                print  "Class State\n" + line
                i += 1
                continue
        
            elif (lineType == classStates["method"]):
                self.stateStack.push(classStates["method"])
                print "Method State\n" + line
                i += 1
                continue

            elif (lineType == classStates["if"]):
                i += 1
                continue

            elif (lineType == classStates["else"]):
                i += 1
                continue

            elif (lineType == classStates["statment"]):
                data = self.stmHand.interprateStatment(line)
                if (len(data[0]) != 0):
                    self.dataSection.append(data[0])
                if (len(data[1]) != 0):
                    self.textSection.append(data[1])
                    i += 1
                    continue
                # self.braceStack.pop()
                if (self.stateStack.isEmpty() != True):
                    self.stateStack.pop()
            i += 1
        self.printMIPSInst()
    
    def printMIPSInst(self):
        print "----------------------- MIPS Generated Code Here -----------------------"
        print self.dataSection
        print self.textSection
        print ".data"
        for data in self.dataSection:
            print data[0]
        print ".text"
        for inst in self.textSection:
            print inst[0]
    def main(self, filepath):
        data = ""
        with open(filepath) as f:
            data = f.read()
        lines = data.split('\n')
        self.interprateLines(lines)

            
    def interprate(self):
        self.main(self.filepath)


if __name__ == '__main__':
    interpreter = Interpretor('./Main.java')
    interpreter.interprate()
